#⚠️ Deprecated ⚠️
This crate is deprecated. All users are encouraged to migrate to [`fat_type`].

#thinbox
`thinbox` provided an alternative implementation of the standard Rust [`Box`] container which had a
pointer-sized representation in all cases.

# Documentation
Inline rustdoc documentation is available. A mirror of this documentation is available at
<https://docs.rs/thinbox>.

# Contributing
`thinbox` was developed at [GitLab].

# License
`thinbox` is licensed under the terms of the [Mozilla Public License, v. 2.0][MPL]. All Source Code
Forms are "Incompatible With Secondary Licenses", as described in *§3.3* of the license.

The corresponding SPDX license identifier is [`MPL-2.0-no-copyleft-exception`][SPDX].

# Copyright
This document is Copyright (C) 2020 Nathan Sharp.

Permission is granted to reproduce this document, in any form, free of charge. The source code form
of this document is subject to the terms of the Mozilla Public License, v. 2.0.

[`fat_type`]: https://crates.io/crates/fat_type
[GitLab]: https://gitlab.com/nwsharp/thinbox
[MPL]: https://mozilla.org/MPL/2.0
[SPDX]: https://spdx.org/licenses/MPL-2.0-no-copyleft-exception.html
