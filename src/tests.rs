//
// Copyright (C) 2020 Nathan Sharp.
//
// This Source Code Form is subject to the terms of the Mozilla Public License,
// v. 2.0. If a copy of the MPL was not distributed with this file, You can
// obtain one at https://mozilla.org/MPL/2.0/
//
// This Source Code Form is "Incompatible With Secondary Licenses", as defined
// by the Mozilla Public License, v. 2.0.
//

use crate::ThinBox;
use std::mem;
use std::mem::MaybeUninit;

trait TestTrait {
    fn get_forty_two(&self) -> usize;
}

#[derive(Clone)]
struct TestStruct {
    pub forty_two: usize,
}

impl TestStruct {
    pub fn new() -> Self {
        Self { forty_two: 42 }
    }
}

impl TestTrait for TestStruct {
    fn get_forty_two(&self) -> usize {
        self.forty_two
    }
}

#[test]
fn thin_sized() {
    let thin = ThinBox::new(TestStruct::new());
    assert_eq!(mem::size_of_val(&thin), mem::size_of::<*mut ()>());
    assert_eq!(thin.get_forty_two(), 42);

    let raw = ThinBox::into_raw(thin);
    let thin2 = unsafe { ThinBox::<TestStruct>::from_raw(raw) };
    assert_eq!(thin2.get_forty_two(), 42);
}

#[test]
fn thin_unsized() {
    let thin: ThinBox<dyn TestTrait> = ThinBox::unsize(TestStruct::new());
    assert_eq!(mem::size_of_val(&thin), mem::size_of::<*mut ()>());
    assert_eq!(thin.get_forty_two(), 42);

    let raw = ThinBox::into_raw(thin);
    let thin2 = unsafe { ThinBox::<dyn TestTrait>::from_raw(raw) };
    assert_eq!(thin2.get_forty_two(), 42);
}

#[test]
fn transmute_into() {
    let thin: ThinBox<dyn TestTrait> = ThinBox::unsize(TestStruct::new());

    let mut test = MaybeUninit::<TestStruct>::uninit();
    let test = unsafe {
        ThinBox::transmute_into(thin, test.as_mut_ptr());
        test.assume_init()
    };

    assert_eq!(test.get_forty_two(), 42);
}

#[test]
fn clone() {
    let thin1 = ThinBox::new(TestStruct::new());
    let thin2 = thin1.clone();

    assert_eq!(thin1.get_forty_two(), 42);
    assert_eq!(thin2.get_forty_two(), 42);

    mem::drop(thin1);

    assert_eq!(thin2.get_forty_two(), 42);
}

#[test]
fn leak_sized() {
    let thin = ThinBox::new(TestStruct::new());
    assert_eq!(thin.get_forty_two(), 42);

    let leaked = ThinBox::leak(thin);
    assert_eq!(leaked.get_forty_two(), 42);

    let unleaked = unsafe { ThinBox::unleak(leaked) };
    assert_eq!(unleaked.get_forty_two(), 42);
}

#[test]
fn leak_unsized() {
    let thin: ThinBox<dyn TestTrait> = ThinBox::unsize(TestStruct::new());
    assert_eq!(thin.get_forty_two(), 42);

    let leaked = ThinBox::leak(thin);
    assert_eq!(leaked.get_forty_two(), 42);

    let unleaked = unsafe { ThinBox::unleak(leaked) };
    assert_eq!(unleaked.get_forty_two(), 42);
}
